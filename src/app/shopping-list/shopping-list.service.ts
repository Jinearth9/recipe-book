import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

import { Ingredient } from '../shared/ingredient';

@Injectable()
export class ShoppingListService {
  startedEditing = new Subject<number>();

  private ingredients: Ingredient[] = [
    { name: 'Apples',   amount: 5 },
    { name: 'Tomatoes', amount: 10 }
  ];

  constructor() { }

  getIngredients(): Observable<Ingredient[]> {
    return of(this.ingredients);
  }

  getIngredient(index: number): Observable<Ingredient> {
    return of(this.ingredients[index]);
  }

  addIngredient(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
  }

  addIngredients(ingredients: Ingredient[]) {
    this.ingredients.push(...ingredients);
  }

  updateIngredient(index: number, newIngredient: Ingredient) {
    this.ingredients[index] = newIngredient;
  }

  deleteIngredient(index: number) {
    this.ingredients.splice(index, 1);
  }
}
