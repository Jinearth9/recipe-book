import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { Recipe } from '../recipes/recipe';

@Injectable()
export class DataStorageService {

  constructor(
    private http: HttpClient
  ) { }

  storeRecipes(recipes: Recipe[]): Observable<any> {
    return this.http.put('https://recipe-book-cc3b2.firebaseio.com/recipes.json', recipes)
      .pipe(
        catchError(this.handleError([]))
      );
  }

  getRecipes(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>('https://recipe-book-cc3b2.firebaseio.com/recipes.json')
      .pipe(
        catchError(this.handleError([]))
      );
  }

  private handleError<T> (result?: T) {
    return (error: HttpErrorResponse): Observable<T> => {
      console.error(error);

      return of(result);
    };
  }
}
