import { Ingredient } from '../shared/ingredient';

export class Recipe {
  name: string;
  description: string;
  imagePath: string;
  ingredients: Ingredient[];
}
