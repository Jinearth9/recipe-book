import { Component, OnInit } from '@angular/core';

import { DataStorageService } from '../../shared/data-storage.service';
import { AuthService } from '../../auth/auth.service';
import { RecipeService } from '../../recipes/recipe.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(
    private dataStorageService: DataStorageService,
    private authService: AuthService,
    private recipeService: RecipeService
  ) { }

  ngOnInit() {
  }

  onSaveData() {
    this.recipeService.getRecipes()
      .subscribe(recipes => {
        this.dataStorageService.storeRecipes(recipes)
          .subscribe();
      });
  }

  onFetchData() {
    this.dataStorageService.getRecipes()
      .subscribe(response => {
        const recipes = response;
        for (const recipe of recipes) {
          if (!recipe['ingredients']) {
            recipe['ingredients'] = [];
          }
        }
        console.log(recipes);
        this.recipeService.setRecipes(recipes);
      });
  }

  onLogout() {
    this.authService.logout();
  }

}
