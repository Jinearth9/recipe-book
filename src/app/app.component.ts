import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyBa27_GKFpthJmgXZx0wTBIlueiJBGYJUY',
      authDomain: 'recipe-book-cc3b2.firebaseapp.com'
    });
  }
}
